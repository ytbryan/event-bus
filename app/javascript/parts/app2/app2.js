import { EventBus } from "../../plugins/event-bus.js";

export default {
  data: function() {
    return {
      message: "Hello app2!"
    };
  },
  methods: {
    update: function() {
      console.log("clicked");
      EventBus.$emit("i-got-clicked", this.message);
    }
  }
};
