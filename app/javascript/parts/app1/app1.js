import { EventBus } from "../../plugins/event-bus.js";

EventBus.$on("i-got-clicked", value => {
  alert("received");
});

export default {
  data: function() {
    return {
      message: "Hello app1!"
    };
  }
};
